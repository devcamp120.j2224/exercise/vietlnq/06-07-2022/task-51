package com.devcamp.s50;

import java.util.ArrayList;

public class ArrayListArray {


    public static void longArrayToIntArrayList() {
        long[] myLongArray = new long[100];
        ArrayList<Long> arrayLongList = new ArrayList<>();
        for (long i : myLongArray) {
            arrayLongList.add(i);
        };
        ArrayList<Integer> arrayIntegerList = new ArrayList<>();
        // Chuyển từ ArrayList Long thành ArrayList Integer:
        for (long i : arrayLongList) {           
            Integer n = (int)i;
            arrayIntegerList.add(n);
        }
        System.out.println("Convert thành công");
    }

    public static void doubleArrayToArrayList() {
        double[] myDoubleArray = new double[100];
        ArrayList<Double> arrayDoubleList = new ArrayList<>();
        for (Double i : myDoubleArray) {
            arrayDoubleList.add(i);
        };
        System.out.println("Successfully converted");
    }


    // Task 51.60
    public static void main(String[] args) {
        //ArrayListArray.longArrayToIntArrayList(); 

        ArrayListArray.doubleArrayToArrayList();
    }
}
