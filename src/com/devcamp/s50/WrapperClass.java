package com.devcamp.s50;

public class WrapperClass {
    Byte mByte;
    Short mShort;
    Integer mInt;
    Long mLong;
    Float mFloat;
    Double mDouble;
    Boolean mBool;
    Character mChar;

    public WrapperClass() {
        mByte = 3;
        mShort = 45;
        mInt = 5674;
        Long l = Long.valueOf(7896);
        mLong = l;
        mFloat = 78.654f;
        mDouble = 89.6785d;
        mBool = true;
        mChar = 'y';
    }

    public void convertToPrimitive() {
        byte newByte = mByte;
        short newShort = mShort;
        int newInt = mInt;
        long newLong = mLong;
        float newFloat = mFloat;
        double newDouble = mDouble;
        boolean newBool = mBool;
        char newChar = mChar;

        System.out.println("Convert successfully");
        System.out.println(newByte);
        System.out.println(newShort);
        System.out.println(newInt);
        System.out.println(newLong);
        System.out.println(newFloat);
        System.out.println(newDouble);
        System.out.println(newBool);
        System.out.println(newChar);
    }

    public void showData() {
        System.out.println(this.mByte);
        System.out.println(this.mShort);
        System.out.println(this.mInt);
        System.out.println(this.mLong);
        System.out.println(this.mFloat);
        System.out.println(this.mDouble);
        System.out.println(this.mBool);
        System.out.println(this.mChar);
    }

    public static void main(String[] args) {
        WrapperClass demo = new WrapperClass();
        demo.convertToPrimitive();
    }
}
