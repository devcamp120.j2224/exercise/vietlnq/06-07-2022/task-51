package com.devcamp.s50;

public class PrimitiveDataType {
    // Gán thuộc tính:
    byte myByte;
    short myShort;
    int myInt;
    long myLong;
    float myFloat;
    double myDouble;
    boolean myBoolean;
    char myChar;

    // Phương thức khởi tạo, gán thuộc tính:
    public PrimitiveDataType() {
        myByte = 3;
        myShort = 5;
        myInt = 589;
        myLong = 43;
        myFloat = 5.78f;
        myDouble = 7.556d;
        myBoolean = false;
        myChar = 'a';
    }

    // Phương thức hiển thị:
    public void showData() {
        System.out.println(this.myByte);
        System.out.println(this.myShort);
        System.out.println(this.myInt);
        System.out.println(this.myLong);
        System.out.println(this.myFloat);
        System.out.println(this.myDouble);
        System.out.println(this.myBoolean);
        System.out.println(this.myChar);
    }

    // Phương thức chuyển từ Primitive type sang Wrapper:
    public void convertToWrapper() {
        Byte newByte = myByte;
        Short newShort = myShort;
        Integer newInteger = myInt;
        Long newLong = myLong;
        Float newFloat = myFloat;
        Double newDouble = myDouble;
        Boolean newBool = myBoolean;
        Character newChar = myChar;

        System.out.println(newByte);
        System.out.println(newShort);
        System.out.println(newInteger);
        System.out.println(newLong);
        System.out.println(newFloat);
        System.out.println(newDouble);
        System.out.println(newBool);
        System.out.println(newChar);
        System.out.println("Convert Successfully");
    }

    public static void main(String[] args) throws Exception {
        // Khởi tạo object:
        PrimitiveDataType demo = new PrimitiveDataType();
        demo.convertToWrapper();
    }
}
