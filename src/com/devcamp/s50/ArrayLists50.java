package com.devcamp.s50;
import java.util.ArrayList;



public class ArrayLists50 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Viet");
        arrayList.add("Linh");
        arrayList.add("Dan");
        arrayList.add("Viet");
        arrayList.add("Nhu");
        
        // 1.
        System.out.println(arrayList.lastIndexOf("Viet"));

        // 2.
        arrayList.add(3, "Phan tu duoc them");
        System.out.println(arrayList);

        // 3.
        System.out.println(arrayList.remove("Dan"));

        // 4.
        System.out.println(arrayList.remove(1));

        // 5.
        System.out.println(arrayList.set(0, "Duoc sua"));
        System.out.println(arrayList);

        // 6.
        System.out.println(arrayList.indexOf("Nhu"));

        // 7.
        System.out.println(arrayList.get(0));

        // 8.
        System.out.println(arrayList.size());

        // 9.
        boolean isExist = arrayList.contains("Viet");
        System.out.println(isExist);

        // 10.
        arrayList.clear();
    }
}
